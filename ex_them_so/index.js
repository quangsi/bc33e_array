var arrNum = [];

function tinhTongSoChan(arr) {
  var result = 0;
  arr.forEach(function (number) {
    if (number % 2 == 0) {
      result += number;
    }
  });
  return result;
}

function demSoLuongSoAm(arr) {
  var count = 0;
  for (var index = 0; index < arr.length; index++) {
    var number = arr[index];
    if (number < 0) {
      count++;
    }
  }
  return count;
}

function hienThiKetQua() {
  // kiểm tra nếu không có giá trị thì sẽ dừng lại
  if (document.querySelector("#txt-number").value.trim() == "") {
    return;
  }
  var number = document.querySelector("#txt-number").value * 1;

  arrNum.push(number);
  document.querySelector("#txt-number").value = "";

  // show kết quả
  document.querySelector("#result").innerHTML = /*html*/ `
   <div>
    <p>  Array : ${arrNum}</p>
    <p> Tổng số chẵn : ${tinhTongSoChan(arrNum)}</p>
    <p> Tổng lượng số âm : ${demSoLuongSoAm(arrNum)}</p>
   </div>
   `;
}
