// primitive value ~ pass by value ~ string, number ,boolean

// reference value ~ pass by reference ~ object , array

var arrSdt = ["123", "456", "678", true, false];
// console.log("arrSdt: ", arrSdt);
// console.log(arrSdt[1]);

var menu = ["Bún bò hệu", "Bún riu", "Bún thịt nướng"];

// length : số lượng phần tử
var tongMonAn = menu.length;
console.log("tongMonAn: ", tongMonAn);

// update giá trị của 1 phần tử dựa vào vị trí  index
menu[0] = "Bánh canh cua";
console.log("menu: ", menu);

// duyệt mảng

// duyệt xuôi
for (var index = 0; index < menu.length; index++) {
  console.log("item", menu[index]);
}

// duyện ngược

for (var index = menu.length - 1; index >= 0; index--) {
  console.log("item", menu[index]);
}

menu.forEach(function (item, index) {
  console.log("item foreach", item, index);
});

// callback

function sayHello(callback, name) {
  console.log("Hi you");
  callback(name);
}

function sayGoodbye(callback, username) {
  console.log("Bye bye");
  callback(username);
}

function introduce(name) {
  console.log("I am " + name);
}

sayHello(function (name) {
  console.log("I am " + name);
}, "Alice");
sayHello(introduce, "Alice");
sayGoodbye(introduce, "Alice");

//  push pop : add remove

// thêm cuối
menu.push("Cơm tấm");
menu.push("Cơm chiên");
console.log("menu: ", menu); // xoá item cuối cùng
menu.pop();
console.log("menu: ", menu);

// unshift shift : thêm đầu xoá đầu

menu.unshift("Phở khô Gia Lai");
menu.shift();
console.log("menu: ", menu);

var viTriComTam = menu.indexOf("Cơm tấmm");

menu.splice(viTriComTam, 1);
console.log("viTriComTam: ", viTriComTam);

// console.log("menu: ", menu);
// menu[viTriComTam] = "Cơm trắng";
// console.log("menu: ", menu);

// slice( start, end) : copy các phần tử từ 1 array sang 1 array mới

var lettes = ["a", "b", "C", "d"];

var coppyArr = lettes.slice(0, 2);
console.log("coppyArr: ", coppyArr);
console.log("lettes: ", lettes);

// splice(start,số lượng) : cắt array => thay đổi giá trị array gốc

lettes.splice(2, 1);
console.log("lettes: ", lettes);

console.log(`



`);
